import sqlite3

connection = sqlite3.connect('data.db')

cursor = connection.cursor() #The cursor is what execute sql queries and then stores the result so we can access it

create_table = "CREATE TABLE users (id int, username text, password text)"
cursor.execute(create_table)

user = (1, 'Connor', 'passConnor')
insert_query = "INSERT INTO users VALUES(?, ?, ?)"
cursor.execute(insert_query, user)

users = [
    (2, 'Amy', 'passConnor'),
    (3, 'Kathleen', 'passConnor'),
    (4, 'Benjamin', 'passConnor'),
    (5, 'Lewis', 'passConnor')
]

cursor.executemany(insert_query, users)

select_query = "SELECT * FROM users"
for row in cursor.execute(select_query):
    print(row)

connection.commit()
connection.close()
